<?php

namespace Drupal\weight_for_custom_fields\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Cache\Cache;

/**
 * Weight For custom Fields settings for this site.
 */
class WeightForCustomFieldsSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'weight_for_custom_fields_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'weight_for_custom_fields.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('weight_for_custom_fields.settings');
    $form['weights'] = [
      '#title' => $this->t('Add Weight with Field Name'),
      '#type' => 'textarea',
      '#description' => $this->t("Enter one field per line with the format form_field_name|weight.(eg) publish|1000. The configured weight will be applied in all the forms if the form field(publish) exists. Higher values will be push down to the bottom, you can also provide form id with it.(eg) taxonomy_term_tags_form|publish|1000. The configured weight will be applied for the form field only in the respective form. If you want a common weight to be applied to a form field in all forms except from one form, then you can need to add the field without formId first publish|1000 and then in the next line add the field with form Id taxonomy_term_tags_form|publish|1000 , you can do the same if you want to have multiple weights for same field in different form. Note: if you add publish|1000 at last  then this feild in all the form will be applied with same weight(1000). Always add common field weights publish|1000 at first, so that form specific  weights (taxonomy_term_tags_form|publish|1000) can override the common weights."),
      '#default_value' => $config->get('weights'),
      '#required' => true,
    ];
    return parent::buildForm($form, $form_state);
  }

  

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('weight_for_custom_fields.settings')
      ->set('weights', $form_state->getValue('weights'))
      ->save();
    Cache::invalidateTags(["weight_for_custom_fields.settings"]);
    parent::submitForm($form, $form_state);
  }

}
